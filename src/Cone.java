// Mykyta Onipchenko, 2034746

public class Cone {
    private double height;
    private double radius;

    public Cone(double height, double radius) {
        this.height = height;
        this.radius = radius;

        if (this.height <= 0) {
            throw new IllegalArgumentException("Height should be > 0");
        }
        if (this.radius <= 0) {
            throw new IllegalArgumentException("Radius should be > 0");
        }
    }

    public double getRadius() {
        return this.radius;
    }

    public double getHeight() {
        return this.height;
    }

    public double getVolume() {
        return Math.PI * this.radius * this.radius * this.height / 3;
    }

    public double getSurfaceArea() {
        return Math.PI * this.radius * (this.radius + Math.sqrt(this.height * this.height + this.radius * this.radius));
    }
}
