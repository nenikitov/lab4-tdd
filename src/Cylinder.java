// Bogdan Ivan, 2032824
public class Cylinder {
    private double height;
    private double radius;

    public Cylinder(double height, double radius){
        this.height = height;
        this.radius = radius;
        
        if(this.height <= 0) {
            throw new IllegalArgumentException("Height cannot be negative");
        }

        if(this.radius <= 0) {
            throw new IllegalArgumentException("Radius cannot be negative");
        }
    }

    public double getHeight(){
        return this.height;
    }

    public double getRadius(){
        return this.radius;
    }

    public double getVolume(){
        return (Math.PI * Math.pow(this.radius, 2) * this.height);
    }

    public double getSurfaceArea(){
        return (Math.PI * 2 * Math.pow(this.radius, 2) + 2 * Math.PI * this.radius * this.height);
    }
}