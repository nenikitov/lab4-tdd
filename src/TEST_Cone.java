import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

// Bogdan Ivan, 2032824

public class TEST_Cone {
    @Test
    public void testConstructor() {
        Cone c = new Cone(3.0, 5.0);
    }

    @Test
    public void testInvalidHeight(){
        try {
            Cone c = new Cone(0.0,2.0);
            fail("No exception!");
        }

        catch(IllegalArgumentException e){
            // Everything works
        }

    }

    @Test
    public void testInvalidRadius(){
        try{
            Cone c = new Cone(1.0, 0.0);
            fail("No exception!");
        }
        catch(IllegalArgumentException e){
            //Everything works
        }
    }

    @Test
    public void testGetHeight(){
        Cone c = new Cone(3.0, 2.0);
        assertEquals(3.0, c.getHeight());
    }

    @Test
    public void testGetRadius(){
        Cone c = new Cone(3.0, 2.0);
        assertEquals(2.0, c.getRadius());
    }

    @Test
    public void testGetVolume(){
        Cone c = new Cone(3.0, 2.0);
        assertEquals(12.57, c.getVolume(), 0.01);
    }

    @Test
    public void testGetSurfaceArea(){
        Cone c = new Cone(3.0, 2.0);
        assertEquals(35.22, c.getSurfaceArea(), 0.01);
    }
}
