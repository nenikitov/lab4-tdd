// Mykyta Onipchenko, 2034746

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class TEST_Cylinder {
    @Test
    public void testValidConstructor() {
        new Cylinder(10, 15);
    }

    @Test
    public void testInvalidConstructorRadius() {
        try {
            new Cylinder(10, 0);
            fail("Should have been an exception");
        }
        catch (IllegalArgumentException e) {
            // All good, the test passes
        }
    }

    @Test
    public void testInvalidConstructorHeight() {
        try {
            new Cylinder(0, 15);
            fail("Should have been an exception");
        }
        catch (IllegalArgumentException e) {
            // All good, the test passes
        }
    }

    @Test
    public void testInvalidConstructorBoth() {
        try {
            new Cylinder(0, 0);
            fail("Should have been an exception");
        }
        catch (IllegalArgumentException e) {
            // All good, the test passes
        }
    }

    @Test
    public void testGetRadius() {
        Cylinder c = new Cylinder(10, 15);
        assertEquals(15, c.getRadius(), 0.001);
    }

    @Test
    public void testGetHeight() {
        Cylinder c = new Cylinder(10, 15);
        assertEquals(10, c.getHeight(), 0.001);
    }

    @Test
    public void testGetVolume() {
        Cylinder c = new Cylinder(10, 15);
        assertEquals(7068.58347, c.getVolume(), 0.001);
    }

    @Test
    public void testGetSurfaceArea() {
        Cylinder c = new Cylinder(10, 15);
        assertEquals(2356.19449, c.getSurfaceArea(), 0.001);
    }
}
